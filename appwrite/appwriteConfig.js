import {Client, Account, Databases} from 'appwrite';

const client = new Client();

client.setEndpoint('http://localhost/v1').setProject("63eb0eb829701f7417b5");

export const account = new Account(client);

export const databases = new Databases(client, "63eb0f0d1d1fb323b5d9");

