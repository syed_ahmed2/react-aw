import React, {useState} from 'react'
import {v4 as uuidv4} from 'uuid'
import {databases} from '../../appwrite/appwriteConfig'

function TodoForm() {
		const [todo, setTodo] = useState("")

		const handleSubmit = (e) => {
				e.preventDefault()
				const promise = databases.createDocument( "63eb0f0d1d1fb323b5d9",
						"63eb0f28481635112156", uuidv4(), {
						todo
				})
				promise.then(
						function(response) {
								console.log(response)
						},
						function(error) {
								console.log(error)
						}
				);
				window.location.reload()
		}

		return (
				<form onSubmit={handleSubmit}>
				<input
				type="text"
				value={todo}
				onChange={(e) => setTodo(e.target.value)}
				/>
				<button type="submit">Add Todo</button>
				</form>
		)
}

export default TodoForm
