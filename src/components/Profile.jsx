import React, { useState } from 'react'
import { useNavigate, Link } from 'react-router-dom'
import {account} from '../../appwrite/appwriteConfig'
import TodoForm from './TodoForm'
import Todos from './Todos'

function Profile() {
		const navigate = useNavigate()

		const [userDetails, setUserDetails] = useState()

		const getUserDetails = async () => {
				const promise = account.get()
				promise.then(
						function(response) {
								setUserDetails(response)
						},
						function(error) {
								console.log(error)
						}
				)
		}

		getUserDetails()


		const logoutUser = async () => {
				const promise = account.deleteSession('current')
				promise.then(
						function(response) {
								navigate('/')
						},
						function(error) {
								console.log(error)
						}
				)
		}

		return (
				<>
				{userDetails ? (
						<>
						<div className="flex flex-col items-center min-h-screen pt-6 
						sm:justify-center 
						sm:pt-0 bg-gray-50">
						<div>
						<a href="/">
						<h3 className="text-4xl font-bold text-purple-600">
						logo
						</h3>
						</a>
						</div>
						<div className="w-full px-6 py-4 mt-6 overflow-hidden bg-white
						shadow-md
						sm:max-w-md sm:rounded-lg">
						<div className="flex flex-col items-center">
						<h1 className="text-3xl font-bold text-gray-900">
						{userDetails.name}
						</h1>
						<p className="text-gray-500">
						{userDetails.email}
						</p>
						</div>
						<div className="flex flex-col items-center mt-6">
						<button
						className="w-full px-4 py-2 text-sm font-medium text-white
						transition duration-500 ease-in-out transform bg-purple-600
						border border-transparent rounded-md hover:bg-purple-700
						focus:outline-none focus:ring-2 focus:ring-offset-2
						focus:ring-purple-500"
						onClick={logoutUser}
						>
						logout
						</button>
						</div>
						</div>
						<div>
						< TodoForm  />
						< Todos />
						</div>
						</div>

						</>
				) : (
						<div className="flex flex-col items-center min-h-screen pt-6
						sm:justify-center
						sm:pt-0 bg-gray-50">
						<div>
						<a href="/">
						<h3 className="text-4xl font-bold text-purple-600">
						logo
						</h3>
						</a>
						</div>
						<div className="w-full px-6 py-4 mt-6 overflow-hidden bg-white
						shadow-md
						sm:max-w-md sm:rounded-lg">
						<div className="flex flex-col items-center">
						<h1 className="text-3xl font-bold text-gray-900">
						loading...
						</h1>
						</div>
						</div>
						</div>
				)}
				</>
		)
}

export default Profile
