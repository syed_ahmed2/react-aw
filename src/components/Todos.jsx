import React, {useEffect, useState} from 'react'
import {databases} from '../../appwrite/appwriteConfig'

function Todos() {
		const [todos, setTodos] = useState()
		const [loader, setLoader] = useState(false)

		useEffect(() => {
				setLoader(true)
				const getTodos= databases.listDocuments("63eb0f0d1d1fb323b5d9", 
						"63eb0f28481635112156")

				getTodos.then(
						function(response) {
								setTodos(response.documents)
								setLoader(false)
						},
						function(error) {
								console.log(error)
						}
				)
				setLoader(false)
		}, [])

		const deleteTodo = (id) => {
				const deleteTodo = databases.deleteDocument("63eb0f0d1d1fb323b5d9",
						"63eb0f28481635112156", id)
				deleteTodo.then(
						function(response) {
								console.log(response)
						},
						function(error) {
								console.log(error)
						}
				)
				window.location.reload()
		}

		return (
				<div className="max-w-7xl mx-auto">
				<p className="text-2xl font-bold mb-2">Todos</p>
				{loader ? ( <p>Loading...</p>
		) : (
			<div>
				{todos && todos.map(item => (
						<div key ={item.$id} >
						<div className="p-4 flex items-center mt-5 justify-between bg-white shadow 
						rounded-lg">
						<div className="flex items-center">
						<div className="ml-4">
						<p className="text-lg font-semibold text-gray-700">{item.todo}</p>
						<button 
						onClick={() => deleteTodo(item.$id)}
						className="bg-red-500 hover:bg-red-700 text-white font-bold py-2 
						px-4 rounded">
						Delete
						</button>

	
				
			</div>
			</div>
			</div>
			</div>
			))}
			</div>
				



		)}
		</div>
		)
}

export default Todos
